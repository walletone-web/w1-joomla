# README #
This module is designed to receive payment orders for CMS Joomla 3.5

=== Wallet One Payment ===
Contributors: h-elena
Tags: Wallet One, Joomla, buy now, payment, payment for Joomla, wallet one integration, Wallet One payment, Virtuemart
Version: 3.2.4
Requires at least: 3.5.1
Tested up to: 3.6.5
Stable tag: 3.6.5
Virtuemart Stable tag: 3.0.18
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for CMS Joomla 3.5. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on CMS Joomla, then you need a module payments for orders made. This will help you plug the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

The latest version of the module you can be found at the link https://bitbucket.org/h_elena/w1-joomla/downloads

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on the site.
3. Instructions for configuring the plugin is in the https://www.walletone.com/ru/merchant/modules/joomla-3-5-virtuemart-3/

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==
= 1.0.0 =
*- Adding a module

= 2.0.0 =
*- Added new universal classes

= 2.0.1 =
*- Fidex bug with checked signature

= 3.0.0 =
*- Fixed bug with anser from calback payment system

= 3.1.0 =
*- Change expired date for invoice(30 days)

= 3.2.0 =
*- Adapt for 54-fz

= 3.2.1 =
*- Add field WMI_CUSTOMER_PHONE

= 3.2.2 =
*- Fix product quantity in order items

= 3.2.3 =
*- Fixed bugs

= 3.2.4 =
*- Fixed escaping symbols in json_encode functions


== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates
